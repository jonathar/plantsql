.PHONY: docker-build docker-run

docker-build:
	docker build -t plantsql .

docker-run:
	docker run --name plantsql --rm plantsql

test-integration:
	docker run --name plantsql --network host --rm plantsql 'postgres://plantsql:secret@localhost:5432/plantsql?sslmode=disable' 

test-image-generation:
	docker run --name plantsql --network host --rm plantsql 'postgres://plantsql:secret@localhost:5432/plantsql?sslmode=disable' | docker run --rm -i jonathar/plantuml -tpng > db_schema_sample.png
