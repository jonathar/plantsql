package generator

import (
	"bytes"
	"fmt"
	"io"
	"net/url"
	"strings"
	"text/template"

	"github.com/pkg/errors"
	"gitlab.com/jonathar/plantsql/pkg"
	"gitlab.com/jonathar/plantsql/pkg/mysql"
	"gitlab.com/jonathar/plantsql/pkg/pg"
)

// Options encapsulates all options needed by the service
type Options struct {
	DbUrl *url.URL
}

// SelectRepository determines the type of repository to use. Currently supports mysql or postgres
func (o *Options) SelectRepository() (Repository, error) {
	if o.DbUrl.Scheme == mysql.SCHEME {
		return mysql.NewRepository(o.DbUrl), nil
	} else if o.DbUrl.Scheme == pg.SCHEME {
		return pg.NewRepository(o.DbUrl), nil
	}
	return nil, fmt.Errorf("Cannot return repository for scheme %s", o.DbUrl.Scheme)
}

// Repository defines the interface all supported database types should support
type Repository interface {
	RetrieveTables() ([]*pkg.TableProperties, error)
	RetrieveForeignKeys(tbls []*pkg.TableProperties) ([]*pkg.ForeignKeyProperties, error)
}

// NewOptions constructs the service options struct
func NewOptions(dbUrl string) (Options, error) {
	u, err := url.Parse(dbUrl)
	if err != nil {
		return Options{}, err
	}
	return Options{u}, nil
}

// Service interface provides PlantUML schema generation
type Service interface {
	Generate(opts Options, w io.Writer) error
}

type service struct{}

// NewService constructs a plantsql.service
func NewService() Service {
	return service{}
}

// Generate orchestrates the generation of a PlantUML schema.
// It scrapes the schema metadata and then generates the PlantUML accordinly
func (service) Generate(opts Options, w io.Writer) error {
	repository, err := opts.SelectRepository()
	if err != nil {
		return err
	}
	tables, err := repository.RetrieveTables()
	if err != nil {
		return err
	}

	fkps, err := repository.RetrieveForeignKeys(tables)
	if err != nil {
		return err
	}

	// printTableProperties(tables)
	// printForeignKeyProperties(fkps)

	tblBuf, err := tablePropertyToPUMLTable(tables)
	if err != nil {
		return err
	}
	fkBuf, err := foreignKeyPropertyToPUMLRelation(fkps)
	if err != nil {
		return err
	}
	err = render(tblBuf, fkBuf, w)
	if err != nil {
		return err
	}
	return nil
}

func tablePropertyToPUMLTable(tbls []*pkg.TableProperties) ([]byte, error) {
	funcMap := template.FuncMap{
		"ToUpper": strings.ToUpper,
	}
	tpl, err := template.New("table").Funcs(funcMap).Parse(pkg.TableTmpl)
	if err != nil {
		return nil, err
	}
	var src []byte
	for _, tbl := range tbls {
		buf := new(bytes.Buffer)
		if err := tpl.Execute(buf, tbl); err != nil {
			return nil, errors.Wrapf(err, "failed to execute template: %s", tbl.Name.String)
		}
		src = append(src, buf.Bytes()...)
	}
	return src, nil
}

func foreignKeyPropertyToPUMLRelation(fkps []*pkg.ForeignKeyProperties) ([]byte, error) {
	tpl, err := template.New("fk").Parse(pkg.FkTmpl)
	if err != nil {
		return nil, err
	}
	var src []byte
	for _, fkp := range fkps {
		buf := new(bytes.Buffer)
		if err := tpl.Execute(buf, fkp); err != nil {
			return nil, errors.Wrapf(err, "failed to execute template: %s", fkp.ConstraintName.String)
		}
		src = append(src, buf.Bytes()...)
	}
	return src, nil
}

func render(tblBuf, fkBuf []byte, out io.Writer) error {
	var src []byte
	src = append([]byte("@startuml\n"+
		"!include https://raw.githubusercontent.com/jonathar/plantuml_styles/main/DB_Schema.puml\n"+
		"LAYOUT_WITH_LEGEND()\n"), tblBuf...)
	src = append(src, fkBuf...)
	src = append(src, []byte("@enduml\n")...)

	if _, err := out.Write(src); err != nil {
		return err
	}
	return nil
}

//nolint
func printTableProperties(tbls []*pkg.TableProperties) {
	for _, tbl := range tbls {
		fmt.Println("TableProperties:", tbl.Name.String, tbl.Description.String)
		for _, c := range tbl.Columns {
			fmt.Printf("\tColumnProperties: %+v\n", c)
		}
	}
}

//nolint
func printForeignKeyProperties(fkps []*pkg.ForeignKeyProperties) {
	fmt.Println("ForeignKeyProperties:")
	for _, fkp := range fkps {
		fmt.Printf("\t %+v\n", fkp)
	}
}
