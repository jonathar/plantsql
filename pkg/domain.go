package pkg

import "database/sql"

// TableProperties defines the contract repositories use to extract data from their data stores and
// the requirements the template engine can use to build PlantUML diagrams.
type TableProperties struct {
	Name, Description sql.NullString
	Columns           []*ColumnProperties
}

// ColumnProperties defines all attributes the template engine needs to build PlantUML diagrams.
type ColumnProperties struct {
	FieldOrdinal                int
	Name, Description, DataType sql.NullString
	NotNull, IsPrimaryKey       sql.NullBool
	IsForeignKey                bool
}

// ForeignKeyProperties defines all attributes the template engine needs to build PlantUML diagrams.
type ForeignKeyProperties struct {
	ChildTable, ChildColumn, ParentTable, ParentColumn, ConstraintName sql.NullString
	IsParentPk, IsChildPk                                              sql.NullBool
}

func (k *ForeignKeyProperties) IsOneToOne() bool {
	switch {
	case k.IsParentPk.Bool && k.IsChildPk.Bool:
		return true
	default:
		return false
	}
}
