package pg

const columnDataSQL = `
SELECT
	a.attnum AS field_ordinal,
	a.attname AS column_name,
	pd.description AS description,
	a.attnotnull AS not_null,
	COALESCE(ct.contype = 'p', false) AS  is_primary_key,
	CASE WHEN a.atttypid = ANY ('{int,int8,int2}'::regtype[])
	AND EXISTS (
	SELECT 1 FROM pg_attrdef ad
	WHERE  ad.adrelid = a.attrelid
	AND    ad.adnum   = a.attnum
	AND    ad.adbin   = 'nextval(''
	|| (pg_get_serial_sequence (a.attrelid::regclass::text
	, a.attname))::regclass
	|| ''::regclass)'
	)
	THEN CASE a.atttypid
	WHEN 'int'::regtype  THEN 'serial'
	WHEN 'int8'::regtype THEN 'bigserial'
	WHEN 'int2'::regtype THEN 'smallserial'
	END
	ELSE format_type(a.atttypid, a.atttypmod)
	END AS data_type
FROM pg_attribute a
JOIN ONLY pg_class c
	ON c.oid = a.attrelid
JOIN ONLY pg_namespace n
	ON n.oid = c.relnamespace
LEFT JOIN pg_constraint ct ON
	ct.conrelid = c.oid AND
	a.attnum = ANY(ct.conkey) AND ct.contype IN ('p', 'u')
LEFT JOIN pg_attrdef ad ON ad.adrelid = c.oid AND ad.adnum = a.attnum
LEFT JOIN pg_description pd ON pd.objoid = a.attrelid AND pd.objsubid = a.attnum
WHERE a.attisdropped = false
AND n.nspname = $1
AND c.relname = $2
AND a.attnum > 0
ORDER BY a.attnum
`

const listTablesSQL = `
SELECT
  c.relname AS table_name,
	  pd.description AS description
		FROM pg_class c
		JOIN ONLY pg_namespace n
		ON n.oid = c.relnamespace
		LEFT JOIN pg_description pd ON pd.objoid = c.oid AND pd.objsubid = 0
		WHERE n.nspname = $1
		AND c.relkind = 'r'
		ORDER BY c.relname
`

const fkDataSQL = `
SELECT
	cl2.relname AS "child_table",
	att2.attname AS "child_column"
	, cl.relname AS "parent_table"
	, att.attname AS "parent_column"
	, con.conname
	, CASE
	WHEN pi.indisprimary IS NULL THEN false
	ELSE pi.indisprimary
	END AS "is_parent_pk"
	, CASE
	WHEN ci.indisprimary IS NULL THEN false
	ELSE ci.indisprimary
	END AS "is_child_pk"
FROM (
	SELECT
	UNNEST(con1.conkey) AS "parent",
	UNNEST(con1.confkey) AS "child",
	con1.confrelid,
	con1.conrelid,
	con1.conname
	FROM pg_class cl
	JOIN pg_namespace ns ON
		cl.relnamespace = ns.oid
	JOIN pg_constraint con1 ON
		con1.conrelid = cl.oid
	WHERE
		ns.nspname = $1 AND con1.contype = 'f'
) con
JOIN pg_attribute att ON
	att.attrelid = con.confrelid AND att.attnum = con.child
LEFT OUTER JOIN pg_index pi ON
	att.attrelid = pi.indrelid AND att.attnum = any(pi.indkey)
JOIN pg_class cl ON
	cl.oid = con.confrelid
JOIN pg_class cl2 ON
	cl2.oid = con.conrelid
JOIN pg_attribute att2 ON
	att2.attrelid = con.conrelid AND att2.attnum = con.parent
LEFT OUTER join pg_index ci ON
	att2.attrelid = ci.indrelid AND att2.attnum = any(ci.indkey)
ORDER BY con.conname
`
