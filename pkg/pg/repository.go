package pg

import (
	"database/sql"
	"net/url"
	"strings"

	_ "github.com/lib/pq"
	"gitlab.com/jonathar/plantsql/pkg"
)

const SCHEME = "postgres"

type pgRepository struct {
	connURL *url.URL
}

func (r pgRepository) dbSchema() string {
	pathParts := strings.Split(r.connURL.Path, "/")
	if len(pathParts) >= 1 {
		return pathParts[1]
	} else {
		return ""
	}
}

// NewRepository constructs a plantsql.pkg.pg.pgRepository
func NewRepository(u *url.URL) pgRepository {
	return pgRepository{u}
}

// RetrieveTables implements the plantsql.pkg.generator.Repository interface for PostgreSQL
func (r pgRepository) RetrieveTables() ([]*pkg.TableProperties, error) {
	db, err := sql.Open("postgres", r.connURL.String())
	if err != nil {
		return nil, err
	}
	defer db.Close()

	tableResults, err := db.Query(listTablesSQL, r.dbSchema())
	if err != nil {
		return nil, err
	}

	var tables []*pkg.TableProperties
	for tableResults.Next() {
		var tp pkg.TableProperties
		err = tableResults.Scan(&tp.Name, &tp.Description)
		if err != nil {
			return tables, err
		}
		tables = append(tables, &tp)
	}

	for _, table := range tables {
		columnResults, err := db.Query(columnDataSQL, r.dbSchema(), table.Name.String)
		if err != nil {
			return nil, err
		}

		var columns []*pkg.ColumnProperties
		for columnResults.Next() {
			var cp pkg.ColumnProperties
			err = columnResults.Scan(&cp.FieldOrdinal, &cp.Name, &cp.Description, &cp.NotNull, &cp.IsPrimaryKey, &cp.DataType)
			if err != nil {
				return nil, err
			}
			columns = append(columns, &cp)
		}
		table.Columns = columns
	}
	return tables, nil
}

// RetrieveForeignKeys implements the plantsql.pkg.generator.Repository interface for PostgreSQL
func (r pgRepository) RetrieveForeignKeys(tbls []*pkg.TableProperties) ([]*pkg.ForeignKeyProperties, error) {
	db, err := sql.Open("postgres", r.connURL.String())
	if err != nil {
		return nil, err
	}
	defer db.Close()

	fkResults, err := db.Query(fkDataSQL, r.dbSchema())
	if err != nil {
		return nil, err
	}

	var fkps []*pkg.ForeignKeyProperties
	for fkResults.Next() {
		var fkp pkg.ForeignKeyProperties
		err = fkResults.Scan(&fkp.ChildTable, &fkp.ChildColumn, &fkp.ParentTable, &fkp.ParentColumn, &fkp.ConstraintName, &fkp.IsParentPk, &fkp.IsChildPk)
		if err != nil {
			return nil, err
		}

		for _, table := range tbls {
			if table.Name.String == fkp.ChildTable.String {
				for _, col := range table.Columns {
					if col.Name.String == fkp.ChildColumn.String {
						col.IsForeignKey = true
					}
				}
			}
		}

		fkps = append(fkps, &fkp)
	}

	return fkps, nil
}
