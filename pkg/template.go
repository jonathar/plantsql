package pkg

const TableTmpl = `
Table({{.Name.String }}, "{{ .Name.String }}") {
{{- if .Description.Valid }}
  {{ .Description.String }}
	..
{{- end }}
{{- range .Columns }}
	{{- if .IsPrimaryKey.Bool }}
	{{ template "pk" . }} {{ .DataType.String | ToUpper }} {{- if .Description.Valid }} : {{ .Description.String }}{{- end }}
	{{- end }}
{{- end }}
{{- range .Columns }}
	{{- if not .IsPrimaryKey.Bool }}
	{{ template "non_pk_field" . }} {{ .DataType.String | ToUpper }} {{- if .Description.Valid }} : {{ .Description.String }}{{- end }}
	{{- end }}
{{- end }}
}

{{- define "fk_base" }}{{- if .IsForeignKey -}}fk({{.Name.String}}){{- else }}{{.Name.String}}{{- end}}{{- end }}
{{- define "pk" }}{{- if .IsPrimaryKey.Bool -}}pk({{- template "fk_base" . }}){{- else }}{{- template "fk_base" . }}{{- end}}{{- end }}
{{- define "not_null" }}{{- if .NotNull.Bool -}}not_null({{.Name.String}}){{- else }}{{.Name.String}}{{- end}}{{- end }}
{{- define "non_pk_field" }}{{- if .IsForeignKey -}}fk({{- template "not_null" . }}){{- else }}{{- template "not_null" . }}{{- end}}{{- end }}
`

const FkTmpl = `
{{ if .IsOneToOne }}{{ .ParentTable.String }} ||..|| {{ .ParentColumn.String }}{{else}}{{ .ParentTable.String }} ||..o{ {{ .ChildTable.String }}{{end}}
`
