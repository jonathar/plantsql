package mysql

import (
	"database/sql"
	"fmt"
	"net/url"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/jonathar/plantsql/pkg"
)

const SCHEME = "mysql"

type mysqlRepository struct {
	connURL *url.URL
}

func (r mysqlRepository) dbSchema() string {
	pathParts := strings.Split(r.connURL.Path, "/")
	if len(pathParts) >= 1 {
		return pathParts[1]
	} else {
		return ""
	}
}

// NewRepository constructs a plantsql.mysql.mysqlRepository
func NewRepository(u *url.URL) mysqlRepository {
	return mysqlRepository{u}
}

// RetrieveTables implements the plantsql.pkg.generator.Repository interface for MySQL
func (r mysqlRepository) RetrieveTables() ([]*pkg.TableProperties, error) {
	connStr := fmt.Sprintf("%s@tcp(%s)%s", r.connURL.User.String(), r.connURL.Host, r.connURL.Path)
	db, err := sql.Open("mysql", connStr)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	tableResults, err := db.Query(listTablesSQL)
	if err != nil {
		return nil, err
	}

	var tables []*pkg.TableProperties
	for tableResults.Next() {
		var tp pkg.TableProperties
		err = tableResults.Scan(&tp.Name)
		if err != nil {
			return tables, err
		}
		tables = append(tables, &tp)
	}

	for _, table := range tables {
		columnResults, err := db.Query(fmt.Sprintf("SHOW FULL COLUMNS IN %s", table.Name.String))
		if err != nil {
			return nil, err
		}

		var columns []*pkg.ColumnProperties
		var ordinal = 0
		for columnResults.Next() {
			var field, datatype, collation, nullablestr, key, defaultvalue, extra, privileges, comment sql.NullString
			err = columnResults.Scan(&field, &datatype, &collation, &nullablestr, &key, &defaultvalue, &extra, &privileges, &comment)
			if err != nil {
				return nil, err
			}

			var notnullable sql.NullBool
			if nullablestr.String == "YES" {
				notnullable = sql.NullBool{Bool: true, Valid: true}
			} else {
				notnullable = sql.NullBool{Bool: false, Valid: true}
			}

			if strings.TrimSpace(comment.String) == "" {
				comment.Valid = false
			}

			datatype.String = strings.ReplaceAll(datatype.String, "(", "[")
			datatype.String = strings.ReplaceAll(datatype.String, ")", "]")

			var isprimarykey sql.NullBool
			var cp = pkg.ColumnProperties{
				FieldOrdinal: ordinal,
				Name:         field,
				Description:  comment,
				DataType:     datatype,
				NotNull:      notnullable,
				IsPrimaryKey: isprimarykey,
			}

			columns = append(columns, &cp)
			ordinal++
		}
		table.Columns = columns
	}
	return tables, nil
}

// RetrieveForeignKeys implements the plantsql.pkg.generator.Repository interface for PostgreSQL
func (r mysqlRepository) RetrieveForeignKeys(tbls []*pkg.TableProperties) ([]*pkg.ForeignKeyProperties, error) {
	connStr := fmt.Sprintf("%s@tcp(%s)%s", r.connURL.User.String(), r.connURL.Host, r.connURL.Path)
	db, err := sql.Open("mysql", connStr)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	fkResults, err := db.Query(fkDataSQL, r.dbSchema())
	if err != nil {
		return nil, err
	}

	var fkps []*pkg.ForeignKeyProperties
	for fkResults.Next() {
		var fkp pkg.ForeignKeyProperties
		err = fkResults.Scan(&fkp.ChildTable, &fkp.ChildColumn, &fkp.ParentTable, &fkp.ParentColumn, &fkp.ConstraintName, &fkp.IsParentPk, &fkp.IsChildPk)
		if err != nil {
			return nil, err
		}

		for _, table := range tbls {
			if table.Name.String == fkp.ChildTable.String {
				for _, col := range table.Columns {
					if col.Name.String == fkp.ChildColumn.String {
						col.IsForeignKey = true
					}
				}
			}
		}

		fkps = append(fkps, &fkp)
	}

	return fkps, nil
}
