# PlantSQL

Generate a ER/Schema diagram for PostgreSQL or MySQL database. Outputs a PlantUML document.

## Example Usage

```
  $> plantsql 'mysql://username:password@tcp(hostname:port)/target_schema'
```

## Development

### Building

```
  $> go build
```

### Running

```
  $> go run .
```

### Testing

```
  $> go test
```

### Integration testing

Start up the integration environment

```
  $> cd integration 
  $> make up
  $> cd ..
  $> make test-integration
```

Shut down the integration environment

```
  $> cd integration 
  $> make down
  $> cd ..
```

### Test image generation

```
  $> make test-image-generation
```
