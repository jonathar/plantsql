module gitlab.com/jonathar/plantsql

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/lib/pq v1.9.0
	github.com/pkg/errors v0.9.1
)
