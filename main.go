package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/jonathar/plantsql/pkg/generator"
)

func init() {
	flag.Usage = func() {
		cmdName := os.Args[0]

		fmt.Fprintf(os.Stderr, fmt.Sprintf("%s: %s", cmdName, "generates a PlantUML doument for a database.\n\n"))
		fmt.Fprintf(os.Stderr, "Usage:\n")
		fmt.Fprintf(os.Stderr, fmt.Sprintf("\t%s DBURL\n", cmdName))
		flag.PrintDefaults()
	}
}

func main() {
	flag.Parse()

	if flag.NArg() < 1 {
		flag.Usage()
		os.Exit(1)
	}

	dbUrl := flag.Arg(0)

	opts, err := generator.NewOptions(dbUrl)
	if err != nil {
		fmt.Fprintf(os.Stderr, fmt.Sprintf("Invalid database url: %s", err))
		os.Exit(1)
	}

	var plantSQLGenerator = generator.NewService()
	err = plantSQLGenerator.Generate(opts, os.Stdout)
	if err != nil {
		panic(err)
	}
}
