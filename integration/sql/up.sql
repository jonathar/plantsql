CREATE schema IF NOT EXISTS plantsql;

CREATE TABLE IF NOT EXISTS plantsql.customers (
  id serial PRIMARY KEY,
  name TEXT NOT NULL DEFAULT ''
);

CREATE TABLE IF NOT EXISTS plantsql.customer_addresses (
  id serial PRIMARY KEY,
  customer_id BIGINT REFERENCES plantsql.customers,
  street_address VARCHAR(255) NOT NULL DEFAULT '',
  city VARCHAR(255) NOT NULL DEFAULT '',
  state VARCHAR(255) NOT NULL DEFAULT '',
  zip VARCHAR(32) NOT NULL DEFAULT ''
)
